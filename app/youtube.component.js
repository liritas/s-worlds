"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var blacklist_1 = require("./blacklist");
var Observable_1 = require("rxjs/Observable");
var BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
var DETAILS_URL = 'https://www.googleapis.com/youtube/v3/videos';
var API_TOKEN = 'AIzaSyAJk1xUI72YYfBMgEc84gjHUX-k2AN6-B0';
var YouTubeAPI = (function () {
    function YouTubeAPI(http) {
        this.http = http;
        this.error = '';
    }
    YouTubeAPI.prototype.search = function (query) {
        var _this = this;
        query += ' мультфильм -' + blacklist_1.blacklist.words.join(' -');
        return this.http.get(BASE_URL + "?q=" + query + "&maxResults=50&order=relevance&part=snippet&relevanceLanguage=ru&type=video&key=" + API_TOKEN)
            .map(function (res) {
            _this.videosJSON = res.json();
            var videos = _this.videosJSON.items;
            var new_videos = [];
            for (var i in videos) {
                var channel = videos[i].snippet.channelId;
                // check ID chanel in blacklist
                if (blacklist_1.blacklist.channels.indexOf(channel) == -1) {
                    // check blacklist words in title and description
                    var title = videos[i].snippet.title;
                    var desc = videos[i].snippet.description;
                    var black_worlds = 0;
                    for (var _i = 0, _a = blacklist_1.blacklist.words; _i < _a.length; _i++) {
                        var word = _a[_i];
                        var reg_word = new RegExp(word, 'i');
                        var title_black_word = title.search(reg_word);
                        var description_black_word = desc.search(reg_word);
                        if (title_black_word > -1 || description_black_word > -1)
                            black_worlds++;
                    }
                    if (black_worlds == 0) {
                        new_videos.push(videos[i]);
                    }
                }
            }
            // Duration filter
            _this.videosJSON.items = new_videos;
            _this.getVideoDetails(new_videos);
            return _this.videosJSON;
        })
            .map(function (json) { return json.items; });
    };
    YouTubeAPI.prototype.getVideoDetails = function (videos) {
        var _this = this;
        this.getVideosFromArray(videos)
            .subscribe(function (res) {
            _this.data = res;
            for (var _i = 0, _a = res.items; _i < _a.length; _i++) {
                var video = _a[_i];
                var dur = video.contentDetails.duration;
                var dat = Date.parse(dur);
                console.log(dat);
            }
        }, function (error) {
            _this.error = 'Error: ' + error;
        });
    };
    YouTubeAPI.prototype.getVideosFromArray = function (videos) {
        var ids = this.getIdsFromArray(videos);
        return this.http.get(DETAILS_URL + "?id=" + ids + "&part=contentDetails&type=video&key=" + API_TOKEN)
            .map(this.extractData)["catch"](this.handleError);
    };
    YouTubeAPI.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    YouTubeAPI.prototype.handleError = function (error) {
        console.log('Error occurred:', error);
        return Observable_1.Observable["throw"](error.message || error);
    };
    YouTubeAPI.prototype.getIdsFromArray = function (arr) {
        var videoIdList = [];
        for (var _i = 0, arr_1 = arr; _i < arr_1.length; _i++) {
            var video = arr_1[_i];
            var videoId = video.id.videoId;
            videoIdList.push(videoId);
        }
        return videoIdList.join();
    };
    return YouTubeAPI;
}());
YouTubeAPI = __decorate([
    core_1.Injectable()
], YouTubeAPI);
exports.YouTubeAPI = YouTubeAPI;
//# sourceMappingURL=youtube.component.js.map