'use strict';

export const blacklist = {
	words: [
		'игра', 'игры', 'игрушка', 'игрушки', 'играем', 'play', 'Видео', 'movie',
		'фигурок', 'фигурки', 'фигурку', 'журнала', 'шоп',
		'сюрприза', 'киндеров', 'opening', 'Киндер', 'Сюрпризы',
		'рисунки', 'нарисовать', 'Раскраска', 'Рисую', 'Самодельный',
		'плэнэт', 'пленет', 'Плэнет', 'планет', 'прохождение', 'породия', 'пародия', 'Прикол', 'Приколы', 'LOL',
		'Transforms', 'Набор', 'arcade', 'Minecraft', 'MINECRAFT', 'Обзор', 'Отрывок',
		'зомби', 'ЗОМБИ', 'УЖАСТИКИ', 'ПСИХИКУ', 'смерт', 'death', 'сумасшедш', 'в шоке', 'влюбилась', 'с ума',
		'LITTLE BIG', 'Клипы', 'клип', 'бэнд', 'Бади', 'обзор',
		'блeaть', 'живодерки', 'напала',
		'Türkçe',
		'трэш', 'ТРЕШ', 'СДОХ', 'приколы', 'халтура', 'плагиат', 'аматори', 'любительское',
		'конкурс', 'сценка',
		'🚺','🚻', '↪'
	],
	channels: [
		'UCzkEf8RNt8wUeOflJsmMwjQ', 'UC9-A6a1acUungyD-2lXC01A', 'UCLSw_aiWwGpZetvr85G_K5Q', 'UCA64_5Qc01lArLzi1MJaoQA', 'UCjaAfs5GC6Mjw1TRon4vHqQ', 'UCcPB1srVbTPK-xXkmnpYTVQ',
		'UC1dNl0ksAONjvi2M3LwgT6A', 'UCtU-3O2ZvXprmesXO9G6JYA', 'UCh4PeR-oIxf1N9EG0nEsWjA', 'UCYxPFqXlBRLyjVcYwxbWWKw', 'UCFxC5uopCnffyakDv64aXqQ', 'UC9GBtDpTE4K214VGNF3ALwg',
		'UCERTiIkimYZzU4ZUzzbqjHA', 'UC6QWAcWix8p3Fq8HA0CVhtQ', 'UCiT_WRptHSwD5Qx2w5rVCVQ', 'UCa5VuGeEk3o57iZzIAurP8w', 'UCqQd_lu8S-_-zuWR9YqfPug', 'UCsA9LmdbtC-X8R2W0wniTcQ',
		'UCU53u_fAEZ-OeV23lNov--Q', 'UCfujDJgZ4aH42sIeRDnsWcQ', 'UCXnJRy-41mHwBoDBfaYHcMg', 'UCK4p9Nut4IRXl44t7bKnuRw', 'UCSH1qvWWRJOhM_N0W2k4LHg', 'UCHhWHqw7MxMHI4PpCyBlNjQ',
		'UCzGS2ZkCQfqj7jt37IvlTBg', 'UCLmGF5YT4mcYziHzJdyqHdA', 'UCkoeEl7LKutnd1KJwsjX3iA', 'UCez4DKBhPyC8xFWCoR8slAA', 'UCs7NafBBcZQh1ayc4zx7tfg', 'UClnX1Ib2RF2Va9_H_-Ktkkw',
		'UCeqYsTwjPjE8GMDE35Nyp3w', 'UCkuHYFJhY7NjJ9ElyJlaj9g', 'UC6n_VQ0qaEHSEJm5OuXrlWw', 'UCqDo1-GcqL0DAztOm3P5OPw', 'UC6pKn48C_TPp8oIQz1zu9vg', 'UCWv7ec9gH6g5bjZ8n3qugIg',
		'UCVIWYVyFFBl7gM1aVID8dQw', 'UCSEve4e1tVAiG7UhAw0VpjQ', 'UCIvqz7M7y6A7EykmKPk9H9Q', 'UCCiyQiFjYN3EJMbw7xKrZRQ', 'UCwQhHEksbVJ8yOkspIinDUw', 'UC0kjjGJTx7gwxoGXmfZ-11w',
		'UCGAT7KHeaz083afc7pYNOZg', 'UCyU6kAJjrQ82z6KI0AEXXzg', 'UCdVmC4dIOdgQttCznFA7zWw', 'UCyJ9Ds0vVGHZERHM3WZkmmw', 'UCgiYYXGjVP2r3yFBSxP5Smw', 'UCfv4TmxNP2i03xLWTJemA3w',
		'UCyIDUAJ05iBaENne86P3_Gw', 'UCNCdeW4f8rGs47zqirIZw5A', 'UCoOXbIMhYxbAjak9bxm1_fw', 'UCgtx7-g4iLYLO4RBeW8Yp6g', 'UC3dNqdtZ5x0OmzliRlcskgw', 'UCU3fglxsmECS7G6XtOytNAQ',
		'UCsZXDIYxMUfmscRePbDXF7w', 'UC-vwj7gf1BoRATlUcFehn4Q', 'UC9C1PVGHc7dTD5jU0SCC4PA', 'UC72cdnjHBPnxyGgnruGcz9w', 'UCmf6jTq5pL5zjk7250teZzQ', 'UCU2fW8mIyWfbsBRWFG7IvyA',
		'UCxKe2iPV7VLly8jRJrip3cg', 'UCD3LBxoKRc5aUIWfCU854CA', 'UCtTDDGLr_0hYxC5B3O8tVSg', 'UCSPcYRwIlIV4NGzmgEh8uZQ', 'UClzmOgDtkrNki8Ps3zjuNUA', 'UCGCQPu9tF0cm8f7NJMSPeew',
		'UCRmDs1TRRzSHdNxtx5Ccp3g', 'UC0BeB2Cznh3vl-yC-ZScc7w', 'UCZOD69EpM_i3JvlHpILQShw', 'UCZVsllsXDhBvRZzBXPG4XeA', 'UCM1HxMPlFntr2Zq8tphpf9A', 'UCG-TBywefoDES6I7Hwo0Sww',
		'UCv7ehxJlH4GzISBWqElxkag', 'UCQ2PG_v4cDc2BMws5Xw887Q', 'UChjnEs3myfRybGfEmslkjtg', 'UCXHrxNAa9hAKDcfyQTs6fug', 'UCA6GVMK4aDeH_M-AeQJKi6Q', 'UCDhNFeEJeXzQBzthm5NLjLg',
		'UCJ4J_tLBSaDN29EoWuEKAAA', 'UCuo9VyowIT-ljA5G2ZuC6Yw', 'UCfyQF4iCspooKRSOshVgaQA', 'UCsa2POR22vaqKGyzYbPCXTQ', 'UCzTk77LQn5bvqdsizpfWcoQ', 'UCJ9tPmlEK8DZchEcHqLKnyQ',
		'UCFrytD3-2zoHyNXJR9vynDw', 'UCYcbOP2wkE5Us1JpWjvxazA', 'UCo_Fseqqq6XjMVqI3YnbTTQ', 'UCHYnbVa2lhxrZjiUi6bEa-g', 'UCz_r01FXbANVXLhEa1dujvw', 'UCcLEENpZ4Nyo0Z5Ce76Larw',
		'UCF8gv_Xd9XICl6_48B2VA6Q', 'UCLPW0Wyd_MS_r3r1x6TnEeA', 'UCyhV71b9asSD_uJtDSqvH0w', 'UCMIu6sujxHRcj5m0IBWGJmg', 'UCKP9BwGLwlq3sx1KUMllbSw', 'UCOzIO5dGMYDWlcZ0yULEsKA',
		'UChFqVPwlG2VRFFRfpPnkVbg', 'UCYsdBEDb1tn1jS7updwiWHQ', 'UCJRIOFy6uW9TlqXpfJROS4g', 'UCZ_PU1b6c7XjHmweGW-s6LQ', 'UCnd7QUDDPWuekB_5h8K-bPw', 'UCl_fBKnC5LOh7neVwMO5XSw',
		'UCcJHWVNfcsMu7frmli3TYpQ', 'UC-LvXctfp9YBIzJJ2VJGPJw', 'UCSX8xdYjvmMD5366kGs3Mbg', 'UCWu-9LzOrcYNJlacQHHrs5Q', 'UCYsK-M86GqcQp_RUAXl8dSw', 'UCnEVRMM01I314XEXPvUK93Q',
		'UCQ8sKceTF2-_rKbDdlAzwqw', 'UCkNeBkz3V0wDXYa77mhW8nA', 'UCs5ersSqGayoPgxY3e9xVMQ', 'UC1yK20sKMkzi88BDuDHqXvA', 'UCvB3bCxkbKPZJaR8MdbnQNg', 'UCqaRt4CN8qQNXfiwYaww0dA',
		'UC-Osc-wnQP-SFAkDEpf44Nw', 'UCxW0sUkn73HyLQHIWF3-lYg', 'UCl6q-edaUsBtJ-fDR3tudnQ', 'UCMAWXdlntIu9tvBMnGIjcBA', 'UCzClVu7a2g4yDY03lgdJXqg', 'UCQ2eMYTiLfVI9-b0JsduWmg',
		'UCU8VAOLPvDjfezg974MOT8A', 'UClGJx4LnJ8gH6pLgB8U7VYQ', 'UCIh52xbmOzriB-7uXyYKcQg', 'UCHF7UhwIlxiJNctlHZZyxcg', 'UCm3wjSTvP7eNAXfTGD4uMeQ', 'UCz3c8qQkBZYOuCFlt5ZsiWA',
		'UCreQstnnsoQVi7axaHHOaGg', 'UCDfFrHnfMPtgmJi6pYQiE3g', 'UCfbx9nsX2yp-4uYhQJ_WrVw', 'UCrsb9ZkOrkPDCBs-9BbxViQ', 'UChOU2c5EsxYWyJpT_i9br1g', 'UC_82QInKX_M19rPvxRejFwA',
		'UCzkvPrdbekmTdJF63_KlDrw', 'UCiZqKO225W_YWvVA-Tz8JOA', 'UCZPJO7_GGDOqz6PGQTTJSgA', 'UC3R3pQKV4laE6yQqJUHXhdw', 'UCWHbomTvN9FGeNDut-uVGYg', 'UC2N7RNKaknS9b2cVbXnsImQ',
		'UCkv2jdcnUBcjlEg0RD3cBTA', 'UCdEywZdqTPtkSSdDI4iXbSw', 'UCMBUiTSDTCK9M4wV-guxJHA', 'UCRkb1I84juew_D0APycCVCA', 'UCJFQJMRhYlGbw9_puoxJXVg', 'UCKM6BPxdaH20QRv6jHPd59A',
		'UCzngNWw9aAmemMFTtvOB80w', 'UCMkjh4T1zC7Hv8fL9fEzFww', 'UCgtUqMACawy2alsveVYZFNg', 'UCCMxXNWx13VCMFsFArA3ORQ', 'UCTR7ymXLbc2aRA-vW8hTYFA', 'UC1pBsOdxMATJP88kBjhdwCg',
		'UCPnckhEEi6BFCAU0aC0PKVw', 'UCzQ0j3jjgleyzVArMFHpl_Q', 'UCVTgsudFIf84g9u4cNCBeOw', 'UCuj_mCNqeEMfg5fxpzGM_ww', 'UCKTwhrE1ce_RAov_t2SKvXg', 'UCXiaYEQWpPKBa44878kUgVw',
		'UCThlJYb5Cip4ObUGO2n1trA', 'UCThlJYb5Cip4ObUGO2n1trA', 'UCBBFYN5bcqWzcYOvzAlkKcQ', 'UC2vQR87LrgxGhNP6u2jGFYw', 'UCZxwztEDa1-bzGDa1u_Xj5Q', 'UCrfMec7g23LBzaqwxMzMRRQ',
		'UCIE6KGNbgglWXKVDLHtWtrw', 'UCrMnRImR50-NOnI1NlzhJjg', 'UCZpt28Ta7n189Gsq_75Pwgg', 'UCIE6KGNbgglWXKVDLHtWtrw', 'UCZpt28Ta7n189Gsq_75Pwgg', 'UCZpt28Ta7n189Gsq_75Pwgg',
		'UCkXa4AnXGRg7hQ5ZJL-NGQg', 'UChHK46PoVm4wBGODuvvvhKg', 'UCqnEsVP73YZ9LYFOdqe9zVQ', 'UCwJiYO9v5yrKuZrI9PN3Wlw', 'UCqTINBgSwqwxbZaXPQMvjoQ', 'UCmVBJoo3YlT_IqDTEgwtAQA',
		'UC-7oaldYGgJ3I7q8gXyP04Q', 'UCsSjhVUhEEAUUf0WJy3h-uQ', 'UCxXr0HYo8Ev3nRfGHC82VWQ', 'UCAP7CMQkJaiGPrK_38YokQw', 'UC5uZFPEMTzJIOQJPVw9nlwA', 'UCo6IHV7hhky3qEhpe3AzTeQ',
		'UCYK4pUdZ5gEwgu2bkvFNn3g', 'UCJNxFCN77I22DffuORlLhhQ', 'UCUDqUVaXXHL32DDyRBCqbHw', 'UCdNvMbDGBqJtGeIj7oAYLzA', 'UC-J_apjS5srOv_qMGao2Uuw', 'UC-J_apjS5srOv_qMGao2Uuw',
		'UCLaKhGjaej0dMVcwG9jVhIw', 'UCJCzYR-p6UUDlzqoTVsLUzw', 'UCrvsNa6Ph0BLf_uX2cCkePQ'
	]
};

export function getUser() {
	let currentUser = JSON.parse(localStorage.getItem('currentUser'));
	if (currentUser === null || currentUser === {}) currentUser = { username: '', role: 'Client' };
	return currentUser;
}

export function setUser(obj) {
	let currentUser = getUser();
	Object.assign(currentUser, obj);
	localStorage.setItem('currentUser', JSON.stringify(currentUser));
}

export function removeUser() {
	localStorage.removeItem('currentUser');
}
