import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }      from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VideoPlayerModule } from './video-player/video-player.module';
import { AppComponent }  from './app.component';

@NgModule({
	imports: [
		BrowserModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		VideoPlayerModule
	],
	declarations: [AppComponent],
	bootstrap: [AppComponent]
})
export class AppModule {
}
