import { Pipe } from '@angular/core';

@Pipe({ name: 'unicodeToDate' })
export class UnicodeToDatePipe {
	transform(value: string, args: string[]): any {
		return new Date(value);
	}
}