"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var youtube_component_1 = require("./youtube.component");
require("rxjs/Rx");
var AppComponent = (function () {
    function AppComponent(youtube) {
        this.youtube = youtube;
        this.search = new forms_1.FormControl();
        this.video_block_show = false;
        this.videoData = "d6xQTf8M51A";
        //observable of results
        this.results = this.search.valueChanges
            .debounceTime(200)
            .switchMap(function (query) { return youtube.search(query); });
    }
    AppComponent.prototype.show_video = function (video_id) {
        this.videoData = video_id;
        this.video_block_show = true;
    };
    AppComponent.prototype.hide_video = function () {
        this.video_block_show = false;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app',
        providers: [youtube_component_1.YouTubeAPI],
        templateUrl: './app/app.component.html'
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map