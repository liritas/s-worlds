import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { YouTubeAPI } from './youtube.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
	selector: 'app',
	providers: [YouTubeAPI],
	templateUrl: './app/app.component.html'
})
export class AppComponent {
	search = new FormControl();
	results: Observable<any>;
	public video_block_show = false;
	videoData: string = "d6xQTf8M51A";

	constructor(public youtube: YouTubeAPI) {
		//observable of results
		this.results = this.search.valueChanges
			.debounceTime(200)
			.switchMap(query => youtube.search(query));
	}

	public show_video(video_id): void {
		this.videoData = video_id;
		this.video_block_show = true;
	}

	public hide_video(): void {
		this.video_block_show = false;
	}
}
