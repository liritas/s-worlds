import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { blacklist } from './blacklist';
import { Observable } from 'rxjs/Observable';

const BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
const DETAILS_URL = 'https://www.googleapis.com/youtube/v3/videos';
const API_TOKEN = 'AIzaSyAJk1xUI72YYfBMgEc84gjHUX-k2AN6-B0';

@Injectable()
export class YouTubeAPI {
	public data;
	public videosJSON;
	public error = '';

	constructor(private http: Http) {
	}

	search(query) {
		query += ' мультфильм -' + blacklist.words.join(' -');
		return this.http.get(`${BASE_URL}?q=${query}&maxResults=50&order=relevance&part=snippet&relevanceLanguage=ru&type=video&key=${API_TOKEN}`)
			.map((res: Response) => {
				this.videosJSON = res.json();
				var videos = this.videosJSON.items;
				var new_videos = [];
				for (var i in videos) {
					let channel = videos[i].snippet.channelId;
					// check ID chanel in blacklist
					if (blacklist.channels.indexOf(channel) == -1) {
						// check blacklist words in title and description
						let title = videos[i].snippet.title;
						let desc = videos[i].snippet.description;
						let black_worlds = 0;
						for (var word of blacklist.words) {
							var reg_word = new RegExp(word, 'i');
							let title_black_word = title.search(reg_word);
							let description_black_word = desc.search(reg_word);
							if (title_black_word > -1 || description_black_word > -1) black_worlds++;
						}
						if (black_worlds == 0) {
							new_videos.push(videos[i])
						}
					}
				}
				// Duration filter
				this.videosJSON.items = new_videos;
				this.getVideoDetails(new_videos);
				return this.videosJSON
			})
			.map(json => json.items);
	}

	public getVideoDetails(videos): void {
		this.getVideosFromArray(videos)
			.subscribe(res => {
					this.data = res;
					for (let video of res.items) {
						let dur = video.contentDetails.duration;
						let dat = Date.parse(dur);
						console.log(dat);
					}
				}, error => {
					this.error = 'Error: ' + error;
				}
			);
	}

	getVideosFromArray(videos): Observable<any> {
		let ids = this.getIdsFromArray(videos);
		return this.http.get(`${DETAILS_URL}?id=${ids}&part=contentDetails&type=video&key=${API_TOKEN}`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
		console.log('Error occurred:', error);
		return Observable.throw(error.message || error);
	}

	private getIdsFromArray(arr) {
		let videoIdList = [];
		for (let video of arr) {
			let videoId = video.id.videoId;
			videoIdList.push(videoId);
		}
		return videoIdList.join();
	}
}