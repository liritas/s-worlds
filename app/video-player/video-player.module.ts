import { Pipe, PipeTransform } from '@angular/core'
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SafePipe } from './video-player.pipe';
import { VideoPlayerComponent } from './video-player.component';

@NgModule({
	imports: [BrowserModule],
	declarations: [VideoPlayerComponent, SafePipe],
	exports: [VideoPlayerComponent]
})

export class VideoPlayerModule {

}