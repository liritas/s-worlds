import { Component, Input } from '@angular/core';

@Component({
	selector: 'video-player',
	templateUrl: '/app/video-player/video-player.component.html'
})
export class VideoPlayerComponent {
	@Input('parentData') videoId: string;
}
